#!/bin/sh


function init()
{
	modprobe libcomposite
	cd /sys/kernel/config/usb_gadget/

	mkdir g1
	cd g1

	echo "0x1a0a" > idVendor
	echo "0xbadd" > idProduct

	mkdir strings/0x409
	echo "1234" > strings/0x409/serialnumber
	echo "Suse" > strings/0x409/manufacturer
	echo "Test" > strings/0x409/product

	mkdir configs/c.1
	mkdir configs/c.1/strings/0x409
	echo "Config1" > configs/c.1/strings/0x409/configuration

	# Setup functionfs
	mkdir functions/ffs.usb0
	ln -s functions/ffs.usb0 configs/c.1

	mkdir -p /run/usb/ffs
	cd /run/usb

	mount usb0 ffs -t functionfs
}

function run()
{
	if [[ $1 == "" ]];then
		buf=512
	else
		buf=$1
	fi

	pid=$(pidof ffs-test)

	if [[ "$pid" != "" ]];then
		kill -9 $pid
	fi

	cd /run/usb/ffs
	ffs-test $buf&

	echo
	echo "pid is: $(pidof ffs-test)"
	echo

	sleep 1

	# Enable the USB device
	echo "musb-hdrc.0" > /sys/kernel/config/usb_gadget/g1/UDC
}

if [[ ! -d /sys/kernel/config/usb_gadget/g1 ]];then
	echo "Creating the device..."
	init
fi

pid=$(pidof ffs-test)

if [[ $pid ]];then
	kill -9 $pid
fi

run $1
